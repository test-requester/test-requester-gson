package com.gitlab.testrequester.gson;

import com.gitlab.testrequester.AbstractJsonMapperTest;
import com.google.gson.Gson;

class GsonJsonMapperTest extends AbstractJsonMapperTest {

    @Override
    protected GsonJsonMapper buildJsonMapper() {
        return new GsonJsonMapper(new Gson());
    }
}
