package com.gitlab.testrequester.gson;

import com.gitlab.testrequester.GenericType;
import com.gitlab.testrequester.JsonMapper;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import lombok.NonNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.function.Supplier;

public class GsonJsonMapper implements JsonMapper {
    private final Gson gson;

    public GsonJsonMapper(@NonNull Gson gson) {
        this.gson = gson;
    }

    @Override
    public byte[] toJsonBytes(@NonNull Object object) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(stream);
        gson.toJson(object, writer);
        writer.flush();
        return stream.toByteArray();
    }

    @Override
    public <T> T fromJson(@NonNull byte[] json, @NonNull Class<T> type) throws IOException {
        return wrapExceptions(() -> {
            InputStreamReader writer = new InputStreamReader(new ByteArrayInputStream(json));
            return gson.fromJson(writer, type);
        });
    }

    @Override
    public <T> T fromJson(@NonNull byte[] json, @NonNull GenericType<T> type) throws IOException {
        return wrapExceptions(() -> {
            InputStreamReader writer = new InputStreamReader(new ByteArrayInputStream(json));
            return gson.fromJson(writer, type.getType());
        });
    }

    private <T> T wrapExceptions(Supplier<T> parser) throws IOException {
        try {
            return parser.get();
        } catch (JsonIOException | JsonSyntaxException e) {
            throw new IOException(e);
        }
    }
}
